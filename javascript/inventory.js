class Inventory {
  constructor() {
    this.articles = [];
  }

  addArticleToInventory(article) {
    this.articles.push(article);
  }

  removeArticleFromInventory(article) {}

  addQuantityArticle(index) {
    this.articles[index].quantity += 1;
  }
  removeQuantityArticle(index) {
    this.articles[index].quantity -= 1;
  }
}
