class User {
  constructor(budget) {
    this.budget = budget;
  }

  checkBudget(cart) {
    if (this.budget < cart.total) {
      alert('Your wallet is empty');
    } else return true;
  }
  pay(cart) {
    if (this.checkBudget(cart) === true) {
      this.budget - cart.total;
      cart.total = 0;
      alert('Thanks for your purchase');
    } else return;
  }
}
