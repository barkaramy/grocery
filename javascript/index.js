document.addEventListener('DOMContentLoaded', async () => {
  let myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');
  myHeaders.append('Accept', 'application/json');
  const inventory = new Inventory();
  const articles = await (await fetch(
    'http://localhost:3000/articles_inventory',
    { headers: myHeaders, mode: 'cors' }
  )).json();
  const users = await (await fetch('http://localhost:3000/users', {
    headers: myHeaders,
    mode: 'cors'
  })).json();

  console.log(articles);
  for (let i = 0; i < articles.length; i++) {
    inventory.addArticleToInventory(articles[i]);
  }
  console.log(inventory);

  const cart = new Cart();
  const user = new User(10);
  const btnAdd = document.getElementById('btn-add');
  const btnRemove = document.getElementById('btn-remove');
  const btnPay = document.getElementById('pay');
  let trblue = null;

  inventory.articles.forEach((element, index) => {
    const inventArticle = document.getElementById('inventory-articles');
    const invenTbody = inventArticle.getElementsByTagName('tbody');
    const trInventory = document.createElement('tr');
    trInventory.setAttribute('id', index);
    trInventory.addEventListener('click', selectInventoryArticle);
    invenTbody[0].appendChild(trInventory).classList.add('article');

    Object.values(element).forEach((item) => {
      const tdInventory = document.createElement('td');
      const itemInventory = document.createTextNode(item);
      tdInventory.appendChild(itemInventory);
      trInventory.appendChild(tdInventory);
    });
  });

  function selectInventoryArticle(e) {
    trblue = e.target.parentNode;

    let allArticles = document.getElementsByClassName('article');

    Array.prototype.forEach.call(allArticles, function(element) {
      element.classList.remove('bg-info');
    });
    trblue.classList.toggle('bg-info');
    btnAdd.classList.toggle('disabled');
  }

  function selectCartArticle(e) {
    trblue = e.target.parentNode;

    let allCartArticles = document.getElementsByClassName('cart-article');

    Array.prototype.forEach.call(allCartArticles, function(element) {
      element.classList.remove('bg-info');
    });
    trblue.classList.toggle('bg-info');
    btnRemove.classList.toggle('disabled');
    btnAdd.classList.toggle('disabled');
  }

  btnAdd.addEventListener('click', () => {
    if (trblue === null) return;
    let index = trblue.getAttribute('id');
    cart.addArticle(articles[index]);
    inventory.removeQuantityArticle(index);
    const cartArticle = document.getElementById('cart-articles');
    let cartTbody = cartArticle.getElementsByTagName('tbody');
    cartTbody[0].innerHTML = '';
    cart.total = 0;
    cart.articles.forEach((element) => {
      const trCart = document.createElement('tr');
      trCart.setAttribute('id', index);
      trCart.addEventListener('click', selectCartArticle);
      cartTbody[0].appendChild(trCart).classList.add('cart-article');
      const td1Cart = document.createElement('td');
      const item1Cart = document.createTextNode(element.name);
      td1Cart.appendChild(item1Cart);
      trCart.appendChild(td1Cart);
      cart.total += element.price;
      let total = document.getElementById('total-price');
      total.innerHTML = cart.total;
      const td2Cart = document.createElement('td');
      const item2Cart = document.createTextNode(element.price);
      td2Cart.appendChild(item2Cart);
      trCart.appendChild(td2Cart);

      const inventArticle = document.getElementById('inventory-articles');
      const invenTbody = inventArticle.getElementsByTagName('tbody');
      invenTbody[0].innerHTML = '';
      inventory.articles.forEach((element, index) => {
        const trInventory = document.createElement('tr');
        trInventory.setAttribute('id', index);
        trInventory.addEventListener('click', selectInventoryArticle);
        invenTbody[0].appendChild(trInventory).classList.add('article');

        Object.values(element).forEach((item) => {
          const tdInventory = document.createElement('td');
          const itemInventory = document.createTextNode(item);
          tdInventory.appendChild(itemInventory);
          trInventory.appendChild(tdInventory);
        });
      });
    });
  });

  btnRemove.addEventListener('click', () => {
    if (trblue === null) return;
    let index = trblue.getAttribute('id');
    console.log(index);
    cart.removeArticle(index);

    trblue.parentNode.removeChild(trblue);
    inventory.addQuantityArticle(index);

    const inventArticle = document.getElementById('inventory-articles');
    const invenTbody = inventArticle.getElementsByTagName('tbody');
    invenTbody[0].innerHTML = '';
    inventory.articles.forEach((element, index) => {
      const trInventory = document.createElement('tr');
      trInventory.setAttribute('id', index);
      trInventory.addEventListener('click', selectInventoryArticle);
      invenTbody[0].appendChild(trInventory).classList.add('article');

      Object.values(element).forEach((item) => {
        const tdInventory = document.createElement('td');
        const itemInventory = document.createTextNode(item);
        tdInventory.appendChild(itemInventory);
        trInventory.appendChild(tdInventory);
      });
    });
  });

  btnPay.addEventListener('click', () => {
    user.pay(cart);
    if (cart.total === 0) {
      let total = document.getElementById('total-price');
      total.innerHTML = cart.total;
      const cartArticle = document.getElementById('cart-articles');
      let cartTbody = cartArticle.getElementsByTagName('tbody');
      cartTbody[0].innerHTML = '';
    }
  });
});
